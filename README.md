# Cherish Conference - Clone App

---

```
Development of this App started with the Flutter Create Contest.

Challenge:
Development began rather quickly and smoothly, yet after a while, development went behind deadline and it became sloppy and I gave up entirely.
Not only did it fail in terms of living up to the requirements but also in terms of build quality.

Current State:
The App is currently getting a complete overhaul and will be better than its predecessor and maybe even get a own spinoff version..
```

[![Codemagic build status](https://api.codemagic.io/apps/5d480de4d711361944321279/5d480de4d711361944321278/status_badge.svg)](https://codemagic.io/apps/5d480de4d711361944321279/5d480de4d711361944321278/latest_build)

## Version:
* Pre-Alpha Release Stage

## Platforms supported:
* Android 6.0+
* iOS (Limited testing)

## Languages currently supported:
* Galactic Common (Terran English)
* High German

* Contributions in other Languages are always welcome, see: [Languages](https://bitbucket.org/jonafeucht/cherish-app-clone/src/master/lib/assets/lang/)

---

## Can I use the Code?
* Yes! I made this Repo explicitly public, so you can learn how this App is build.
* If you need code snippets, feel free to take what you need, as is stated in the License - no need to link me.
* This Repo will be maintained until further notice.
* Just clone the Repo and get started!
* Only thing you can't do is make money off of this App. If you do, you better lawyer up! Cause I'm not coming back for 30%, I'm coming back for everything!

---

![Index](https://i.imgur.com/gMB60Nk.png width=300)

