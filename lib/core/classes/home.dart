import 'package:cherish/core/classes/firstTab.dart';
import 'package:cherish/core/classes/secondTab.dart';
import 'package:cherish/core/classes/thirdTab.dart';
import 'package:cherish/core/classes/welcome.dart';
import 'package:cherish/pages/settings.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new DynamicTheme(
        defaultBrightness: Brightness.light,
        data: (brightness) => new ThemeData(
              primarySwatch: Colors.indigo,
              brightness: brightness,
            ),
        themedWidgetBuilder: (context, theme) {
          return new MaterialApp(
              localizationsDelegates: [
                // todo: create App Localization
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              // todo: better support for locale
              supportedLocales: [
                const Locale('en'), // English
                const Locale('de'), // German
                // ... other locales the app supports
              ],
              debugShowCheckedModeBanner: false,
              // todo: dynamic title
              title: 'HOME',
              theme: theme,
              home: new MyHomePage());
        });
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // todo: Build Stream

    // todo: Either RC or Database

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.black,
      appBar: AppBar(
          title: Text("WELCOME"),
          iconTheme: IconThemeData(color: Colors.white, size: 30),
          actions: <Widget>[
            InkWell(
              child: Icon(FontAwesomeIcons.cog),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => Settings()));
              },
            ),
            SizedBox(width: 30)
          ]),
      drawer: SizedBox(
        width: 400,
        child: Drawer(
          child: new Container(
            color: Colors.amber[200],
            child: new ListView(
              children: <Widget>[
                new DrawerHeader(
                  decoration: new BoxDecoration(color: Colors.amber[200]),
                  child: new Center(
                    child: new Text(
                      "CHURCH Conference",
                      style: TextStyle(
                          fontSize: 30, fontFamily: "PxGroteskRegular-Bold"),
                    ),
                  ),
                ),
                new ListTile(
                  title: new Text(
                    "CHURCH 2020",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontFamily: "PxGroteskRegular"),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new welcomeTab()));
                  },
                ),
                new ListTile(
                  title: new Text(
                    "Arena Map",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontFamily: "PxGroteskRegular"),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new welcomeTab()));
                  },
                ),
                new ListTile(
                  title: new Text(
                    "Info Desk",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontFamily: "PxGroteskRegular"),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new welcomeTab()));
                  },
                ),
                new ListTile(
                  title: new Text(
                    "Prayer & Praise",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontFamily: "PxGroteskRegular"),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new welcomeTab()));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      body: new SingleChildScrollView(
        child: new Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.only(bottom: 50),
                child: Image.network(
                  'https://www.lifechurchhome.com/wp-content/uploads/2018/05/ch19main.jpg',
                ),
              ),
              //      v3
              new YoutubePlayer(
                context: context,
                videoId: "QdZFHGpTsr4",
                flags: YoutubePlayerFlags(
                  autoPlay: false,
                  showVideoProgressIndicator: true,
                ),
                videoProgressIndicatorColor: Colors.red,
              ),
              /*    v2
                        new YoutubePlayer(
                          context: context,
                          videoId: "QdZFHGpTsr4",
                          autoPlay: false,
                          showVideoProgressIndicator: true,
                          videoProgressIndicatorColor: Colors.red,
                          progressColors: ProgressColors(
                            playedColor: Colors.white,
                            handleColor: Colors.white,
                          ),
                          onPlayerInitialized: (controller) {},
                        ),
                        */

              new Container(
                child: new Padding(
                  padding: EdgeInsets.all(10.0),
                  child: new RaisedButton(
                    color: Colors.amber[200],
                    elevation: 4.0,
                    splashColor: Colors.amber,
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => FirstTab()));
                    },
                    child: new SizedBox(
                      height: 50.0,
                      width: 300.0,
                      child: new Center(
                        child: const Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: Text("W E L C O M E",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 30.0,
                                  fontFamily: "PxGroteskRegular")),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: new Padding(
                  padding: EdgeInsets.all(10.0),
                  child: new RaisedButton(
                    color: Colors.red,
                    elevation: 4.0,
                    splashColor: Colors.red[900],
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => secondTab()));
                    },
                    child: new SizedBox(
                      height: 50.0,
                      width: 300.0,
                      child: new Center(
                        child: const Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: Text('Y O U T H',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30.0,
                                  fontFamily: "PxGroteskRegular")),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: new Padding(
                  padding: EdgeInsets.all(10.0),
                  child: new RaisedButton(
                    color: Colors.white,
                    elevation: 4.0,
                    splashColor: Colors.white30,
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => thirdTab()));
                    },
                    child: new SizedBox(
                      height: 50.0,
                      width: 300.0,
                      child: new Center(
                        child: const Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: Text('Y U N G A D U L T S',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 30.0,
                                  fontFamily: "PxGroteskRegular")),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: new Padding(
                  padding: EdgeInsets.all(10.0),
                  child: new RaisedButton(
                    color: Colors.white,
                    elevation: 4.0,
                    splashColor: Colors.white30,
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => thirdTab()));
                    },
                    child: new SizedBox(
                      height: 50.0,
                      width: 300.0,
                      child: new Center(
                        child: const Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: Text('A D U L T S',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 30.0,
                                  fontFamily: "PxGroteskRegular")),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
