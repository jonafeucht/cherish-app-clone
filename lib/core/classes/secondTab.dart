import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

// Music: Sarah McMillan - King Of My Heart

class secondTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return YoutubeScaffold(
      fullScreenOnOrientationChange: true,
      child: Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text("YOUTH"),
        iconTheme: IconThemeData(color: Colors.white, size: 30),
      ),
      body: new SingleChildScrollView(
        child: new Container(
          child: new Center(
            child: new Column(
              // center the children
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[

                // v3
                new YoutubePlayer(
                  context: context,
                  videoId: "nCxqjEz5hJ4",
                  flags: YoutubePlayerFlags(
                    autoPlay: false,
                    showVideoProgressIndicator: true,
                  ),
                  videoProgressIndicatorColor: Colors.red,
                ),

                /* Youtube v2
                new YoutubePlayer(
                  context: context,
                  videoId: "nCxqjEz5hJ4",
                  showVideoProgressIndicator: false,
                  hideControls: false,
                  autoPlay: false,
                  onPlayerInitialized: (controller) {},
                ), */

                new Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: new Text(
                    "How do you catch your breath when you feel breathless?",
                    style: new TextStyle(color: Colors.white, fontSize: 17.0),
                  ),
                ),
                new Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: new Text(
                    "Where do you go when you are running out of air, running wild, running on empty? How do you learn to breathe again? Inhaling life, exhaling truth, finding the breath to bring dry bones to life, refuelling the should and invigorating the mind. Run to the breath that has the power to awaken dormant dreams and lift the weariest of hearts.",
                    style: new TextStyle(color: Colors.white, fontSize: 17),
                  ),
                ),
                new Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.0),
                  child: new Text(
                    "It’s His breath in our lungs that gives us our very life. His breath changes everything and can permeate all things. So go ahead and breathe Him in and learn to breathe again.",
                    style: new TextStyle(color: Colors.white, fontSize: 17),
                  ),
                ),
                new Text(
                  "By Jona Feucht",
                  textAlign: TextAlign.center,
                  style: new TextStyle(color: Colors.white, fontSize: 17),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
