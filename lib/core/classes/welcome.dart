import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';



// Music: Autograf - You might be
class welcomeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,

      appBar: AppBar(
        title: Text("ABOUT APP"),        
      ),

      body: new Container(
        child: new Center(
          child: new Column(
            // center the children
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Text(
                "VERSION:",
                style: new TextStyle(color: Colors.black,fontSize: 30,fontFamily: "PxGroteskRegular-Bold"),
              ),
              new Text(
                "0.0.1-a Pre-Alpha",
                style: new TextStyle(color: Colors.black,fontSize: 20,fontFamily: "PxGroteskRegular"),
              ),
              new Text(
                "Developed with Jesus & Love", textAlign: TextAlign.center,
                style: new TextStyle(color: Colors.black,fontSize: 20,fontFamily: "PxGroteskRegular"),
              ),
              new Text(
                "By Jona Feucht", textAlign: TextAlign.center,
                style: new TextStyle(color: Colors.black,fontSize: 20,fontFamily: "PxGroteskRegular"),
              ),
              new InkWell(
              child: Text("https://jonafeucht.de", style: new TextStyle(fontSize: 20.0,fontFamily: "PxGroteskRegular"),
              ),
                    onTap: () async { launch('https://jonafeucht.de/?ref=app');
                    },),

            ],
          ),
        ),
      ),
    );
  }
}