import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';



// Music: Elohim - Hillsong Worship

class FirstTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.black,

      appBar: AppBar(
        title: Text("WELCOME"),
        iconTheme: IconThemeData(color: Colors.white, size: 30),
        
      ),

      body: new SingleChildScrollView(
      child: new Container(
        child: new Center(
          child: new Column(
            // center the children
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              
              new Image.network(
                'https://www.lifechurchhome.com/wp-content/uploads/2018/05/ch19main.jpg',
              ),

              new Padding(
                padding: EdgeInsets.symmetric(vertical: 10.0),
              child: new Text(
                "VERSION:",
                style: new TextStyle(color: Colors.white,fontSize: 30),
              ),
              ),
              new Text(
                "0.0.1-a Pre-Alpha",
                style: new TextStyle(color: Colors.white,fontSize: 20),
              ),
              new Text(
                "Developed with Jesus & Love", textAlign: TextAlign.center,
                style: new TextStyle(color: Colors.white,fontSize: 20),
              ),
              new Text(
                "By Jona Feucht", textAlign: TextAlign.center,
                style: new TextStyle(color: Colors.white,fontSize: 20),
              ),
              new InkWell(
              child: Text("https://jonafeucht.de", style: new TextStyle(fontSize: 20.0)),
                    onTap: () async { launch('https://jonafeucht.de/?ref=app');
                    },),

            ],
          ),
        ),
      ),
      ),
    );
  }
}