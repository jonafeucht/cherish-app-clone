import 'package:flutter/material.dart';
import 'package:intercom_flutter/intercom_flutter.dart';

// For test purposes only

class InterCom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new SingleChildScrollView(
            child: new Center(
                child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        new FlatButton(
          child: Text('Open Intercom'),
          onPressed: () async {
            await Intercom.displayMessenger();
          },
        ),
      ],
    ))));
  }
}
