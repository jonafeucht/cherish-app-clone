/*
Copyright 2019 Jona Tadashi Feucht

MIT LICENSE
*/
import 'package:cherish/core/classes/home.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:intercom_flutter/intercom_flutter.dart';

final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

void main() async {
    // todo: await Intercom.initialize('appIdHere', iosApiKey: 'iosKeyHere', androidApiKey: 'androidKeyHere');
    runApp(Home());

    // todo FIREBASE CLOUD MESSAGING aka. NOTIFICATION
        /*
       _firebaseMessaging.configure(
          onMessage: (Map<String, dynamic> message) {
            print('on message $message');
          },
          onResume: (Map<String, dynamic> message) {
            print('on resume $message');
          },
          onLaunch: (Map<String, dynamic> message) {
            print('on launch $message');
          },
        );*/
}